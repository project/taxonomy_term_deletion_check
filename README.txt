
-- SUMMARY --

The Taxonomy Term Deletion Check module checks if the taxonomy term is 
associated to a node or a child term prior to deletion.

The checking occurs when you are in the edit term page.  If the selected 
term is associated to a node or a term, the delete button is disabled 
(greyed out) with an explanation on the side.

Note: This module only prevents the deletion of a term in the UI, it will 
not react to programmatically deleted terms.

-- REQUIREMENTS --

Taxonomy module (core).

-- INSTALLATION --

* Install as usual.

-- CONTRIBUTORS --

Maintainer
- 'abrlam' <https://www.drupal.org/u/abrlam>

Based on the following article:
Prevent deletion of taxonomy term if associated with a node
http://gbyte.co/blog/prevent-deletion-taxonomy-term-if-associated-node
